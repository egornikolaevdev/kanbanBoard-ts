import { RootState } from '../../redux/store/store'
import { DragDropContext, DropResult, OnDragEndResponder } from 'react-beautiful-dnd'
import { useSelector, useDispatch } from 'react-redux'

import { enterDesc, enterID, setFieldsClear, enterName } from '../../redux/features/taskSlice/newTaskCreateSlice'
import { 
  addToQueue, 
  removeFromQueue, 
  setInProgress,
  removeFromInProgress, 
  setInCheck, 
  removeFromInCheck, 
  setInDone, 
  removeFromInDone} from '../../redux/features/taskSlice/taskMoveSlice'
import './kanban-board.css'
import Column from '../Column/Column'
import { ITask } from '../../types/TaskTypes/ITask'

export default function Kanbanboard() {
  const dispatch = useDispatch();
  //Достаем состояния полей ввода 
  let taskID = useSelector((state:RootState) => state.newTaskCreateSlice.taskID)
  let taskName = useSelector((state:RootState) => state.newTaskCreateSlice.taskName)
  let taskDesc = useSelector((state:RootState) => state.newTaskCreateSlice.taskDesc)
  //Достаем состояния колонок
  const inQueue:ITask[] = useSelector((state:RootState) => state.taskMoveReducer.inQueue)
  const inProgress:ITask[] = useSelector((state:RootState) => state.taskMoveReducer.inProgress)
  const inCheck:ITask[] = useSelector((state:RootState) => state.taskMoveReducer.inCheck)
  const inDone:ITask[] = useSelector((state:RootState) => state.taskMoveReducer.inDone)
  //Функции на окончание драга
  const handleDragEnd:OnDragEndResponder = (result:DropResult) => {
    const {destination, source, draggableId } = result;
    if (source.droppableId === destination?.droppableId){
      return
    }
    if (destination?.droppableId === undefined){
      return
    }
    //Функционал удаления задач из колонок при перетаскивании
    if (source.droppableId === '0'){
      dispatch(removeFromQueue(removeItemByID(Number(draggableId), inQueue)))
    }
    if (source.droppableId === '1'){
      dispatch(removeFromInProgress(removeItemByID(Number(draggableId), inProgress)))
    }
    if (source.droppableId === '2'){
      dispatch(removeFromInCheck(removeItemByID(Number(draggableId), inCheck)))
    }
    //Получение задачи по ID
    const task = findItemByID(Number(draggableId), [...inQueue, ...inProgress, ...inCheck])
    //Функционал перемещение задач по колонкам
    if (task){
      if (destination?.droppableId === '0'){
        dispatch(addToQueue({...task}))
      }
      if (destination?.droppableId === '1'){
        dispatch(setInProgress({...task}))
      }
      if (destination?.droppableId === '2'){
        dispatch(setInCheck({...task}))
      }
      if (destination?.droppableId === '3'){
        dispatch(setInDone({...task}))
      }
    }
    
  }
  //Поиск задачи по ID в указанном массиве
  function findItemByID(id:number | string, array:ITask[]){
    return array.find((item) => item.id === id) as ITask
  }
  //Удаление задачи по ID из указанного массива
  function removeItemByID(id:number | string, array:ITask[]){
    return array.filter((item) => item.id !== id)
  }
  //Удаление выбранной задачи
  function deleteTask(id:number){
    const task=findItemByID(id, [...inQueue, ...inProgress, ...inCheck, ...inDone]) 
    if (task){
      if (task.status === 'Q'){
        dispatch(removeFromQueue(removeItemByID(id, inQueue)))
      }
      if (task.status === 'P'){
        dispatch(removeFromInProgress(removeItemByID(id, inProgress)))
      }
      if (task.status === 'C'){
        dispatch(removeFromInCheck(removeItemByID(id, inCheck)))
      }
      if (task.status === 'D'){
        dispatch(removeFromInDone(removeItemByID(id, inDone)))
      }
    }
    
  }
  //Перемещение выбранной задачи в колонку Завершено
  function finishTask(id:number){
    const task=findItemByID(id, [...inQueue, ...inProgress, ...inCheck, ...inDone])
    if (task){
      if (task.status === 'Q'){
        dispatch(removeFromQueue(removeItemByID(id, inQueue)))
        dispatch(setInDone(findItemByID(id, inQueue)))
      }
      if (task.status === 'P'){
        dispatch(removeFromInProgress(removeItemByID(id, inProgress)))
        dispatch(setInDone(findItemByID(id, inProgress)))
      }
      if (task.status === 'C'){
        dispatch(removeFromInCheck(removeItemByID(id, inCheck)))
        dispatch(setInDone(findItemByID(id, inCheck)))
      }
      if (task.status === 'D'){
        return
      } 
    }
  }
  //Проверка на повторение ID задачи при добавлении
  function checkTask(id:number):boolean{
    if (findItemByID(id, [...inQueue, ...inProgress, ...inCheck, ...inDone ]) === undefined){
      console.log(`Добавлена задача с ID ${id}`)
      return true
    }else{
      alert(`Задача с ID ${id} уже существует`)
      return false
    }
  }
  //Действие для отправки формы
  const handleSubmit = (event:React.ChangeEvent<HTMLFormElement>) => {
    event.preventDefault()
    dispatch(setFieldsClear())
    if (isNaN(Number(taskID))){
      alert('ID задачи должно быть числом.')
      return
    }
    if ((taskID.length === 0) || (taskID==='0')){
      alert('Введите корректное значение ID')
      return
    }
    if (checkTask(Number(taskID))){
      dispatch(addToQueue({id:Number(taskID), title:taskName, desc:taskDesc, status:'Q'}))
    }else{
      return
    }
  }
  //Изменение поля ID задачи
  const handleChangeID = (event:React.ChangeEvent<HTMLInputElement>) => {
    dispatch(enterID(event.target.value))
  }
  //Изменение поля Название задачи
  const handleChangeName = (event:React.ChangeEvent<HTMLInputElement>) => {
    dispatch(enterName(event.target.value))
  }
  //Изменение поля Описание задачи
  const handleChangeDesc = (event:React.ChangeEvent<HTMLInputElement>) => {
    dispatch(enterDesc(event.target.value))
  }
  return (
    <DragDropContext onDragEnd={handleDragEnd}>
      <div className='kanban-board'>
        <h2 className='style-header'>Kanban Board</h2>
          <form 
            className='field-group-container'
            onSubmit={handleSubmit}
          >
            <input 
              type='search'
              value={taskID}
              className='style-input'
              placeholder='ID' 
              onChange={handleChangeID}
            />
            <input 
              type='search'
              value={taskName}
              className='style-input' 
              placeholder='Название' 
              onChange={handleChangeName}
            />
            <input 
              value={taskDesc}
              type='search'
              className='style-input' 
              placeholder='Описание' 
              onChange={handleChangeDesc}
            />
            <button 
              className='primary-button' 
              type='submit'
            >
              Добавить
            </button>
          </form>
        <div className='column-container'>
          <Column 
            columnTitle='В очереди' 
            tasks={inQueue} 
            columnID='0' 
            deleteTask={deleteTask}
            finishTask={finishTask}
          />
          <Column 
            columnTitle='В работе' 
            tasks={inProgress} 
            columnID='1' 
            deleteTask={deleteTask}
            finishTask={finishTask}
          />
          <Column 
            columnTitle='На проверке' 
            tasks={inCheck} 
            columnID='2' 
            deleteTask={deleteTask}
            finishTask={finishTask}
          />
          <Column 
            columnTitle='Завершены'
            tasks={inDone} 
            columnID='3' 
            deleteTask={deleteTask}
            finishTask={finishTask}
          />
        </div>
      </div>
    </DragDropContext>
  )
}