import { useSelector } from 'react-redux'
import { RootState } from '../../redux/store/store'
import { Draggable, DraggableProvided, DraggableStateSnapshot } from 'react-beautiful-dnd'
import './task.css'
import { TaskProps } from '../../types/TaskTypes/TaskProps'

export default function Task({task, index, deleteTask, finishTask}:TaskProps) {
  const taskID = useSelector((state:RootState) => state.newTaskCreateSlice.taskID)
  const taskIsDone = task.status === 'D'
  return(    
    <Draggable draggableId={`${task.id}`} key={taskID} index={index} isDragDisabled={taskIsDone ? true : false}>
      {(provided: DraggableProvided, snapshot: DraggableStateSnapshot) => (
      <div 
        className={taskIsDone ? 'task-card-completed' : 'task-card'}
        {...provided.draggableProps}
        {...provided.dragHandleProps}
        ref={provided.innerRef}
      >
        <p className='id-label'>{task.id}</p>
        <p className='name-label'>{task.title}</p>
        <p className='desc-label'>{task.desc}</p>
        <div className='buttons-container'>
          <button 
            disabled={taskIsDone ? true : false}
            className={taskIsDone ? 'secondary-button-disabled' : 'secondary-button'}
            onClick={() => finishTask(task.id)}
          >
            Завершить
          </button>
          <button 
            className={taskIsDone ? 'delete-button-completed' : 'delete-button'} 
            onClick={() => deleteTask(task.id)}
          >
            Удалить
          </button>
        </div>
      </div>
      )}
    </Draggable>
  )
}