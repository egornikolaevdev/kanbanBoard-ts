import { useEffect } from "react";
import './app.css'
import { fetchData } from "../../data/data";
import Kanbanboard from "../KanbanBoard/Kanbanboard";

export default function App(){
  //Фейковый запрос на загрузку данных с сервера
  useEffect(()=>{fetchData()}, [])
  return(
    <div className='app'>
      <Kanbanboard/>
    </div>
  );
}