import {Droppable, DroppableProvided} from 'react-beautiful-dnd'
import './column.css'
import { ITask } from '../../types/TaskTypes/ITask'
import Task from '../Task/Task'
import { ColumnProps } from '../../types/ColumnType/ColumnProps'

export default function Column({columnTitle, columnID, tasks, deleteTask, finishTask}:ColumnProps) {
  return (
    <div className='container'>
      <h3 className='column-header'>{columnTitle}</h3>
      <Droppable droppableId={columnID}>
        {(provided:DroppableProvided) => (
        <div 
          className='task-list'
          ref={provided.innerRef}
          {...provided.droppableProps}
          >
          {tasks.map((task:ITask, index: number) => (
            <Task 
              key={index} 
              index={index} 
              task={task} 
              deleteTask={deleteTask}
              finishTask={finishTask}
            />
          ))}
            {provided.placeholder}
        </div>
        )}
      </Droppable>
    </div>
  )
}