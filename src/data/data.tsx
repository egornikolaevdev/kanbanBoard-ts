//Получение данных из сети
export const fetchData = async() => {
  try {
    const response = await fetch('https://kanban-board-reworked-default-rtdb.europe-west1.firebasedatabase.app/.json')
    const jsonData = await response.json()

    if (response.ok) {
    console.log('Данные успешно загружены', response.status)
    const data = JSON.stringify(jsonData)
    return data
    }else{
      console.error('Ошибка при загрузке данных', response.status)
    }
  }catch (error) {
    console.error('Ошибка при выполнении запроса', error)
  return
  }
};