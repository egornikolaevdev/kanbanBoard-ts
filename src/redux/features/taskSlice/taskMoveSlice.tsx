import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { ITask } from '../../../types/TaskTypes/ITask';

//Исходные состояния всех колонок
const inQueue:ITask[] = [
  {
    id: 1,
    title: 'Отчет',
    desc: 'Отправить электронное письмо со сводкой проекта команде до конца дня.',
    status:'Q'
  },
  {
    desc: 'Составить список покупок и сходить в магазин после работы.',
    id: 2,
    title: 'Покупки',
    status:'Q'
  },
  {
    desc: 'Подготовить заявку на отпуск и согласовать ее с руководством.',
    id: 3,
    title: 'Отпуск',
    status:'Q'
  }
]
const inProgress:ITask[] = []
const inCheck:ITask[] = []
const inDone:ITask[] = []
export const taskMoveSlice = createSlice({
  name:'taskSlice',
  initialState:{
    inQueue:inQueue,
    inProgress:inProgress,
    inCheck:inCheck,
    inDone:inDone
  },
  reducers:{
    //Добавление задачи в колонку inQueue
    addToQueue:(state, action:PayloadAction<ITask>) => {
      const newTask:ITask = {
        id:action.payload.id, 
        title:action.payload.title !== '' ? action.payload.title : `Задача ${action.payload.id}`,
        desc: action.payload.desc,
        status:'Q'
      }
      state.inQueue.push(newTask)
    },
    //Удаление задачи из колонки inQueue
    removeFromQueue:(state ,action:PayloadAction<ITask[]>) => {
      state.inQueue = action.payload
    },
    //Добавление задачи в колонку inProgress
    setInProgress:(state, action:PayloadAction<ITask>) => {
      const newTask:ITask = {
        id:action.payload.id,
        title:action.payload.title,
        desc: action.payload.desc,
        status:'P'
      }
      state.inProgress.push(newTask)
    },
    //Удаление задачи из колонки inProgress
    removeFromInProgress:(state ,action:PayloadAction<ITask[]>) => {
      state.inProgress = action.payload
    },
    //Добавление задачи в колонку inCheck
    setInCheck:(state, action:PayloadAction<ITask>) => {
      const newTask:ITask = {
        id:action.payload.id,
        title:action.payload.title, 
        desc: action.payload.desc,
        status:'C'
      }
      state.inCheck.push(newTask)
    },
    //Удаление задачи из колонки inCheck
    removeFromInCheck:(state ,action:PayloadAction<ITask[]>) => {
      state.inCheck = action.payload
    },
    //Добавление задачи в колонку inDone
    setInDone:(state, action:PayloadAction<ITask>) => {
      const newTask:ITask = {
        id:action.payload.id, 
        title:action.payload.title, 
        desc: action.payload.desc, 
        status:'D'
      }
      state.inDone.push(newTask)
    },
    //Удаление задачи из колонки inDone
    removeFromInDone:(state ,action:PayloadAction<ITask[]>) => {
      state.inDone = action.payload
    },
  }
})

export const {
  addToQueue, 
  removeFromQueue, 
  setInProgress, 
  removeFromInProgress, 
  setInCheck, 
  removeFromInCheck, 
  setInDone, 
  removeFromInDone} = taskMoveSlice.actions;
export default taskMoveSlice.reducer;