import { PayloadAction, createSlice } from "@reduxjs/toolkit";

export const newTaskCreateSlice = createSlice({
  name:'newTaskCreateSlice',
  initialState:{
    taskID:'',
    taskName:'',
    taskDesc:''
  },
  reducers:{
    enterID:(state, action: PayloadAction<string>) => {
      state.taskID = action.payload
    },
    enterName:(state, action:PayloadAction<string>) => {
      state.taskName = action.payload
    },
    enterDesc:(state, action:PayloadAction<string>) => {
      state.taskDesc = action.payload
    },
    setFieldsClear:(state)=> {
      state.taskID = ''
      state.taskName = ''
      state.taskDesc = ''
      
    },
  }
})

export const {enterID, setFieldsClear, enterName, enterDesc} = newTaskCreateSlice.actions
export default newTaskCreateSlice.reducer