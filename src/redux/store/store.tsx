import { configureStore } from "@reduxjs/toolkit"
import taskMoveReducer from '../features/taskSlice/taskMoveSlice'
import newTaskCreateSlice from "../features/taskSlice/newTaskCreateSlice"

const store = configureStore({
  reducer:{
    taskMoveReducer:taskMoveReducer,
    newTaskCreateSlice: newTaskCreateSlice,
  }
})

export type RootState = ReturnType<typeof store.getState>
export default store