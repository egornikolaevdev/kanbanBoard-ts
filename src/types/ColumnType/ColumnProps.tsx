import { ITask } from "../TaskTypes/ITask"

// columnTitle - название колонки
//columnID - уникальный идентификатор колонки
//tasks - массив задач 
//deleteTask - функция удаления задачи
//finishTask - функция переноса задачи сразу в последнюю колонку
export type ColumnProps = {
  columnTitle:string,
  columnID:string,
  tasks:ITask[],
  deleteTask(id:number):void,
  finishTask(id:number):void,
}