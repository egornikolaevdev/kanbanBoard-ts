import { ITask } from "./ITask"

//task - задача
//index
//deleteTask - функция удаления задачи
//finishTask - функция переноса задачи сразу в последнюю колонку
export type TaskProps = {
  task:ITask,
  index: number, 
  deleteTask(id:number):void,
  finishTask(id:number):void
}