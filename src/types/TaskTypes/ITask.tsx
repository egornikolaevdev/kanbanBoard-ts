//id - уникальный идентификатор задачи
//title - заголовок задачи, по умолчанию будет `Задача ${id}`
//desc - более подробное описание задачи
//status - Q: в очереди(inQueue), P: в работе(inProgress), C: на проверке(inCheck), D: задача завершена(inDone)

export interface ITask{
  id:number,
  title?:string,
  desc?:string,
  status: 'Q' | 'P' | 'C' | 'D'
}